const express = require('express');

const app = express();

app.use(express.static('public'));

const server = app.listen(4100, ()=> {
  console.log('server is running on port 4100');
});
